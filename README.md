# Candidate Takehome Exercise

This is a simple backend engineer take-home test to help assess candidate skills and practices. We appreciate your interest in Voodoo and have created this exercise as a tool to learn more about how you practice your craft in a realistic environment. This is a test of your coding ability, but more importantly it is also a test of your overall practices.

If you are a seasoned Node.js developer, the coding portion of this exercise should take no more than 2-3 hours to complete. Depending on your level of familiarity with Node.js, Express, and Sequelize, it may not be possible to finish in 3 hours, but you should not spend more than 4 hours.

The theory portions of this test are more open-ended. It is up to you how much time you spend addressing these questions. We recommend spending less than 1 hour.

For the record, we are not testing to see how much free time you have, so there will be no extra credit for monumental time investments. We are looking for concise, clear answers that demonstrate domain expertise.

# Project Overview

This project is a simple game database and consists of 2 components.

The first component is a VueJS UI that communicates with an API and renders data in a simple browser-based UI.

The second component is an Express-based API server that queries and delivers data from an SQLite data source, using the Sequelize ORM.

This code is not necessarily representative of what you would find in a Voodoo production-ready codebase. However, this type of stack is in regular use at Voodoo.

# Project Setup

You will need to have Node.js, NPM, and git installed locally. You should not need anything else.

To get started, initialize a local git repo by going into the root of this project and running `git init`. Then run `git add .` to add all of the relevant files. Then `git commit` to complete the repo setup. You will send us this repo as your final product.

Next, in a terminal, run `npm install` from the project root to initialize your dependencies.

Finally, to start the application, navigate to the project root in a terminal window and execute `npm start`

You should now be able to navigate to http://localhost:3000 and view the UI.

You should also be able to communicate with the API at http://localhost:3000/api/games

If you get an error like this when trying to build the project: `ERROR: Please install sqlite3 package manually` you should run `npm rebuild` from the project root.

# Practical Assignments

Pretend for a moment that you have been hired to work at Voodoo. You have grabbed your first tickets to work on an internal game database application.

#### FEATURE A: Add Search to Game Database

The main users of the Game Database have requested that we add a search feature that will allow them to search by name and/or by platform. The front end team has already created UI for these features and all that remains is for the API to implement the expected interface. The new UI can be seen at `/search.html`

The new UI sends 2 parameters via POST to a non-existent path on the API, `/api/games/search`

The parameters that are sent are `name` and `platform` and the expected behavior is to return results that match the platform and match or partially match the name string. If no search has been specified, then the results should include everything (just like it does now).

Once the new API method is in place, we can move `search.html` to `index.html` and remove `search.html` from the repo.

#### FEATURE B: Populate your database with the top 100 apps

Add a populate button that calls a new route `/api/games/populate`. This route should populate your database with the top 100 games in the App Store and Google Play Store.
To do this, our data team have put in place 2 files at your disposal in an S3 bucket in JSON format:

- https://interview-marketing-eng-dev.s3.eu-west-1.amazonaws.com/android.top100.json
- https://interview-marketing-eng-dev.s3.eu-west-1.amazonaws.com/ios.top100.json

# Theory Assignments

You should complete these only after you have completed the practical assignments.

The business goal of the game database is to provide an internal service to get data for all apps from all app stores.  
Many other applications at Voodoo will use consume this API.

#### Question 1:

We are planning to put this project in production. According to you, what are the missing pieces to make this project production ready?
Please elaborate an action plan.

At first, the needed elements to be considered as production-ready will be:

- A Dockerfile to encapsulate the app in a container
- An health check endpoint
- Setup a CI/CP to build, test and deploy the app
- Create the infrastructure with terraform : database, kubernetes service, service account
- Create two environments (staging and prodution)
- Add logs
- Monitoring and alerting
- Swagger documentation
- Authentication layer
- Add typescript to the project

Dependanding on the available time to push the project in production, theses elements could be an element of the first release or develop after the first beta release.

- Complete the tests for each endpoint / feature to be covered by unit or integration tests.
- Replace SQLite database to a dedicated database instance (eg Postgresql, can create index columns on the name to better performance on searching)
- Detailled Documentation
- Fine tuning alerting

In the first instance, I recommend to not provide the service for the all internal clients. We have to select key stakeholders as a beta testers to the service. Ensure the service is stable with few applications, get feedbacks from these stakeholders about the current service and key features needed for them.
After the first beta, we could open the service for the other applications. Ensure that the service is still stable with more users. And develop in parallel new feature according to the needs.

#### Question 2:

Let's pretend our data team is now delivering new files every day into the S3 bucket, and our service needs to ingest those files every day through the populate API. Could you describe a suitable solution to automate this?
Feel free to propose architectural changes.

At the first and if the files are still with "acceptable" size, we can setup a lambda function to be triggered on S3 event when the files are put in the S3 service. The lambda function will call the API endpoint. And the file will be ingested.

If the files get bigger, we must use a dedicated job to process the files (due to the process time and memroy limit). To trigger the job, we can use a simple kubernetes cronjob to start the job to a recurrent time. Or a more cleaver solution is to use Apache Airflow to detect the file on S3.

#### Question 3:

Both the current database schema and the files dropped in the S3 bucket are not optimal.
How would you improve them?

Database schema:

- Create a table for redundant information: platform, publisher (use foreign keys)
- Change the column id to a UUID
- Create an index on the name or the platform
- Can create a table for games and link to game_platforms (specific platform informaiton: plateform, storeId, bundleId, appVersion, isPublished)

Files:

- Flat the files (the current is an a array of array with 3 elements inside) to get only one array
- Remove redundant attributes (ex: icon/iconUrl)
- Uniform attribute name case (mix between camelCase and snake case)
- Remove plateform specific information or move them to a specific object attribute (ios or android)
- Avoid ambiguous name (app_id vs id are the same id?)
- Avoid null data
