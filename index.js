const express = require('express');
const bodyParser = require('body-parser');
const SequelizeOp = require('sequelize').Op;
const db = require('./models');
const { fetchAndParseTopGames } = require('./services/FetchTopGamesService');

const app = express();

app.use(bodyParser.json());
app.use(express.static(`${__dirname}/static`));

app.get('/api/games', (req, res) =>
  db.Game.findAll()
    .then((games) => res.send(games))
    .catch((err) => {
      console.log('There was an error querying games', JSON.stringify(err));
      return res.send(err);
    })
);

app.post('/api/games', (req, res) => {
  const {
    publisherId,
    name,
    platform,
    storeId,
    bundleId,
    appVersion,
    isPublished,
  } = req.body;
  return db.Game.create({
    publisherId,
    name,
    platform,
    storeId,
    bundleId,
    appVersion,
    isPublished,
  })
    .then((game) => res.send(game))
    .catch((err) => {
      console.log('***There was an error creating a game', JSON.stringify(err));
      return res.status(400).send(err);
    });
});

app.delete('/api/games/:id', (req, res) => {
  // eslint-disable-next-line radix
  const id = parseInt(req.params.id);
  return db.Game.findByPk(id)
    .then((game) => game.destroy({ force: true }))
    .then(() => res.send({ id }))
    .catch((err) => {
      console.log('***Error deleting game', JSON.stringify(err));
      res.status(400).send(err);
    });
});

app.put('/api/games/:id', (req, res) => {
  // eslint-disable-next-line radix
  const id = parseInt(req.params.id);
  return db.Game.findByPk(id).then((game) => {
    const {
      publisherId,
      name,
      platform,
      storeId,
      bundleId,
      appVersion,
      isPublished,
    } = req.body;
    return game
      .update({
        publisherId,
        name,
        platform,
        storeId,
        bundleId,
        appVersion,
        isPublished,
      })
      .then(() => res.send(game))
      .catch((err) => {
        console.log('***Error updating game', JSON.stringify(err));
        res.status(400).send(err);
      });
  });
});

app.post('/api/games/search', (req, res) => {
  const { name, platform } = req.body;

  const whereFilter = {
    ...(platform && { platform }),
    ...(name && {
      name: {
        [SequelizeOp.like]: `%${name}%`,
      },
    }),
  };

  return db.Game.findAll({ where: whereFilter })
    .then((games) => res.send(games))
    .catch((error) => {
      console.error('Error during fetching games', JSON.stringify(error));
      res.send(error);
    });
});

app.post('/api/games/populate', async (req, res) => {
  try {
    const { ios, android } = await fetchAndParseTopGames();
    await db.Game.bulkCreate([...ios, ...android]);
  } catch (err) {
    console.error('Error during populating the games', err);
  }

  res.status(200).send();
});

app.listen(3000, () => {
  console.log('Server is up on port 3000');
});

module.exports = app;
