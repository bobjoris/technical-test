const axios = require('axios');

const baseUrl =
  'https://interview-marketing-eng-dev.s3.eu-west-1.amazonaws.com/';
const filenames = {
  ios: 'ios.top100.json',
  android: 'android.top100.json',
};

const fetchAndParseTopGames = async () => {
  const [ios, android] = await Promise.all([
    download('ios'),
    download('android'),
  ]);

  return {
    ios: sliceTo100(ios).map((game) => parse(game)),
    android: sliceTo100(android).map((game) => parse(game)),
  };
};

const sliceTo100 = (games) => games.flat().slice(0, 100);

const parse = (game) => ({
  publisherId: game['publisher_id'],
  name: game.name,
  platform: game.os,
  storeId: game['app_id'],
  bundleId: game['bundle_id'],
  appVersion: game.version,
  isPublished: true,
});

const download = async (platform) => {
  try {
    return (await axios.get(`${baseUrl}${filenames[platform]}`)).data;
  } catch (error) {
    console.error('Error while fetching json file', JSON.stringify(error));
  }
};

module.exports = {
  fetchAndParseTopGames,
};
